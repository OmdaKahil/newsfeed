const fs = require("fs");
const path = require("path");

const getAllDirectories = function (dirPath) {
  let arrayofDirectories = [],
    files = fs.readdirSync(dirPath);
  files.forEach(function (file) {
    if (fs.statSync(dirPath + "/" + file).isDirectory()) {
      arrayofDirectories.push(file);
    }
  });
  return arrayofDirectories;
};

const getAllFiles = function (dirPath, arrayOfFiles) {
  files = fs.readdirSync(dirPath);
  arrayOfFiles = [];
  files.forEach(function (file) {
    if (fs.statSync(dirPath + "/" + file).isDirectory()) {
      arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles);
    } else {
      arrayOfFiles.push(path.join(__dirname, dirPath, "/", file));
      arrayOfFiles.push(path.join(file));
    }
  });
  return arrayOfFiles;
};

const capitalize = (str) => {
  const arr = str.split(" ");
  for (var i = 0; i < arr.length; i++) {
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
  }
  let str2 = arr.join(" ");
  str2 = str2.replace(/\s/g, "");
  return str2;
};

const camelCase = (str) => {
  return str
    .replace(/\s(.)/g, function ($1) {
      return $1.toUpperCase();
    })
    .replace(/\s/g, "")
    .replace(/^(.)/, function ($1) {
      return $1.toLowerCase();
    });
};

const Camelcase = (str) => {
  return str
    .replace(/\s(.)/g, function ($1) {
      return $1.toLowerCase();
    })
    .replace(/\s/g, "")
    .replace(/^(.)/, function ($1) {
      return $1.toUpperCase();
    });
};

module.exports = {
  getAllDirectories,
  getAllFiles,
  capitalize,
  camelCase,
  Camelcase,
};

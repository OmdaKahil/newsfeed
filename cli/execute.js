'use strict';
const fs = require('fs');
const colors = require('cli-color');
const red = colors.xterm(9);
const greenYellow = colors.xterm(154);
var figlet = require('figlet');
let generals = require('./utils/generals');

const {
  createContainerTemplate,
  createComponentTemplate,
} = require('./templates');
const askQuestion = require('./questions/questions');

let basicQuestionsResult = [];
let directoryPath = '../app';
let directories = ['Create your file here', 'create a new folder'];
let choosenDirectory = {};
let directoryName = {};

const getBasicQuestionsResult = async () => {
  basicQuestionsResult = await askQuestion.askBasicQuestions();
};

const getDirectoryQuestionsResult = async () => {
  directoryName = await askQuestion.createDirectoryQuestions();
};

const getAllDirectories = path => {
  directories = generals.getAllDirectories(path);

  directories.push('Create your file here');
  directories.push('create a new folder');
};

const getDirection = async directories => {
  choosenDirectory = await askQuestion.directionQuestions(directories);
};

async function createContainer(name, path) {
  const template = createContainerTemplate(name);
  fs.writeFile(
    path + `/${generals.camelCase(name) + basicQuestionsResult.EXTENSION}`,
    template,
    function (err) {
      if (err) throw err;
      console.log(greenYellow('File Created!'));
    },
  );
}

async function createComponent(name, path) {
  const template = createComponentTemplate(name);
  fs.writeFile(
    path + `/${generals.camelCase(name) + basicQuestionsResult.EXTENSION}`,
    template,
    function (err) {
      if (err) throw err;
      console.log(greenYellow('File Created!'));
    },
  );
}

async function createDirectory(name, path) {
  fs.mkdirSync(path + `/${name}`, function (err) {
    if (err) throw err;
  });
}

const competeProcess = async dirPath => {
  getAllDirectories(dirPath);
  if (directories.length !== 0) await getDirection(directories);
  if (directories.length === 0) {
    if (basicQuestionsResult.Type === 'container') {
      createContainer(basicQuestionsResult.FILE_NAME, directoryPath);
    } else if (basicQuestionsResult.Type === 'component') {
      createComponent(basicQuestionsResult.FILE_NAME, directoryPath);
    }
  } else if (choosenDirectory.Choosen_directory === 'create your file here') {
    if (basicQuestionsResult.Type === 'container') {
      createContainer(basicQuestionsResult.FILE_NAME, directoryPath);
    } else if (basicQuestionsResult.Type === 'component') {
      createComponent(basicQuestionsResult.FILE_NAME, directoryPath);
    }
  } else if (choosenDirectory.Choosen_directory === 'create a new folder') {
    await getDirectoryQuestionsResult();
    while (directoryName.DIRECTORY_NAME === '') {
      console.log(red('directory name cannot be empty'));
      await getDirectoryQuestionsResult();
    }
    await createDirectory(
      generals.camelCase(directoryName.DIRECTORY_NAME),
      directoryPath,
    );
    directoryPath += `/${generals.camelCase(directoryName.DIRECTORY_NAME)}/`;
    await competeProcess(directoryPath);
  } else {
    directoryPath += `/${choosenDirectory.Choosen_directory}/`;
    await competeProcess(directoryPath);
  }
};

const init = () => {
  figlet('N E W F E E D S ! !\n\n', function (err, data) {
    if (err) {
      console.log('Something went wrong...');
      console.dir(err);
      return;
    }
    console.log(data);
  });
};

async function main() {
  await getBasicQuestionsResult();
  while (basicQuestionsResult.FILE_NAME === '') {
    console.log(red('The name of your file should not be empty'));
    await getBasicQuestionsResult();
  }
  getAllDirectories(directoryPath);
  await competeProcess(directoryPath);
}

init();
setTimeout(() => {
  main();
}, [10]);

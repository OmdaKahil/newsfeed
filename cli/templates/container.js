let generals = require('../utils/generals');

const createContainerTemplate = containerName => {
  return `import React from 'react';
  import { View, Text, StyleSheet } from 'react-native';
  
  const ${generals.capitalize(containerName)} = () => {
    return (
      <View style={styles.container}>
        <Text>${generals.capitalize(containerName)}</Text>
      </View>
    );
  };

  const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
      flex: 1,
      justifyContent: 'center',
    },
  });
  
  export { ${generals.capitalize(containerName)} };
  `;
};

module.exports = createContainerTemplate;

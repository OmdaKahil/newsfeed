let generals = require("../utils/generals");

const createComponentTemplate = (componentName) => {
  return `import React from 'react';
    import { View, Text, StyleSheet } from 'react-native';
    
    interface Props {
      /** A text to be displayed in the center of this component */
      title: string;
    }
    
    const ${generals.capitalize(componentName)} = (props: Props) => {
      const { title } = props;
      return (
        <View style={styles.conatiner}>
          <Text>{title}</Text>
        </View>
      );
    };
    
    const styles = StyleSheet.create({
      conatiner: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
      },
    });
    
    export { ${generals.capitalize(componentName)} };
    `;
};

module.exports = createComponentTemplate;

const createComponentTemplate = require("./component");
const createContainerTemplate = require("./container");

module.exports = {
  createComponentTemplate,
  createContainerTemplate,
};

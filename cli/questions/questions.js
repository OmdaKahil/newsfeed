"use strict";
const inquirer = require("inquirer");
const colors = require("cli-color");
const Magenta2 = colors.xterm(165);
const Magenta3 = colors.xterm(164);
const Magenta4 = colors.xterm(163);
const DeepPink3 = colors.xterm(162);
const DeepPink4 = colors.xterm(161);
const Red = colors.xterm(9);

const askBasicQuestions = () => {
  const questions = [
    {
      type: "input",
      name: "FILE_NAME",
      message: Magenta2(
        "What is the name of the file you want to create *without extension*?"
      ),
      filter: function (val) {
        return val.toLowerCase();
      },
    },
    {
      type: "list",
      name: "EXTENSION",
      message: Magenta3("What is the file extension?"),
      choices: [".tsx", ".jsx"],
      filter: function (val) {
        return val;
      },
    },
    {
      type: "list",
      name: "Type",
      message: Magenta4("Choose type for your file"),
      choices: ["component", "container"],
      filter: function (val) {
        return val;
      },
    },
  ];
  return inquirer.prompt(questions);
};

const directionQuestions = (FOLDERSLIST) => {
  const directionQuestion = [
    {
      type: "list",
      name: "Choosen_directory",
      message: DeepPink3(
        "Please choose in which folder you want the file to be created"
      ),
      choices: FOLDERSLIST,
      filter: function (val) {
        return val.toLowerCase();
      },
    },
  ];
  return inquirer.prompt(directionQuestion);
};

const createDirectoryQuestions = (FOLDERSLIST) => {
  const directoryQuestions = [
    {
      type: "input",
      name: "DIRECTORY_NAME",
      message: DeepPink4("Insert name for your directory"),

      filter: function (val) {
        return val.toLowerCase();
      },
    },
  ];
  return inquirer.prompt(directoryQuestions);
};

module.exports = {
  askBasicQuestions,
  directionQuestions,
  createDirectoryQuestions,
};

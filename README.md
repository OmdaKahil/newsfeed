## Description

- React native template, Using the Object oritented principles, type safety, and the cool features that typescript provide us

## Table of content

- [Commands](#markdown-header-commands)
- [How to run](#markdown-header-how-to-run)
  - [Android](#markdown-header-android)
  - [iOs](#markdown-header-ios)
- [Structure](#markdown-header-structure)
  - [components](#markdown-header-components)
  - [containers](#markdown-header-containers)
  - [components/styled](#markdown-header-components-styled)
  - [assets](#markdown-header-assets)
  - [api](#markdown-header-api)
  - [redux](#markdown-header-redux)
    - [redux/slices](#markdown-header-redux-actions)
    - [redux/actions](#markdown-header-redux-reducers)
    - [redux/types](#markdown-header-redux-constants)
  - [utils](#markdown-header-utils)
  - [config](#markdown-header-config)
  - [cli](#markdown-header-cli)
- [Coding Conventions](#markdown-header-coding-conventions)
- [Features](#markdown-header-features)
- [Suggestions](#markdown-header-suggestions)
  - [VS-Code extentions](#markdown-header-vs-code-extentions)

## Commands

|       command        |                           action                           |
| :------------------: | :--------------------------------------------------------: |
|    `yarn install`    |                 Download the dependencies                  |
|     `yarn start`     |                     Start the bundler                      |
| `yarn android:clean` |                 Clean the android project                  |
| `yarn android:build` |                   Creates a release apk                    |
|      `yarn ios`      |         Run a debugging version of the app on iOS          |
|  `yarn ios:install`  |    Excuted the pod install command in the ios directory    |
|       `cd cli`       |                    switch to CLI folder                    |
|    `node execute`    | execute the cli to create folders-components or containers |

## How to run

#### Android

- At root directory run `yarn` or `yarn install` to install the dependencies.
- In the `android` directory add `local.properties` and add your own sdk location on your machine
- react-native run-android

#### iOS

- Run `yarn install`.
- Then run `react-native run-ios` to run the project on iOS device or run it using your xcode.

## Structure

The project root directory structure is as follows:

```
  '|-- <root>',
  '    |-- cli',
  '    |-- app',
  '        |-- assets',
  '        |-- components',
  '        |-- containers',
 '        |-- config',
 '        |-- navigator',
  '        |-- redux',
  '        |-- store',
  '        |-- utils',
  ''
```

- [`/cli`](cli) for the interactive cli that generates project template files.
- [`/app`](app) for all source files
- [`app/assets`](app/assets) for assets (images,colors,fonts,localization(string containing each landguage) etc ...)
- [`app/config`](app/config) for configuration files (permissions,translation, etc ...)
- [`app/containers`](app/features) for project features (newsFeed,Descriptions ..)
- [`app/navigator`](app/navigator) for (navigation between screens ..)
- [`app/store`](app/store) for redux configurations (combineReducers, middlewares, persist etc ...)
- [`app/components`](app/components) for multiple use stateless styled components
- [`app/utils`](src/utils) for utils used throughout the project

#### features

writing logic into containers and components for user interface interaction using redux to manage state that should be used into multiple screens instead of switching objects into params, and storing data using the redux store.
redux is managed as follow:
types : to create the style of the object
Actions : for making api calls and fetching data
reducers : holding slices and firing functions. Dispatch actions using thunk then return if the response has been fullfield panding or rejected

Example

```ts
// localStorage.slice.tsx
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import { LocalStorage } from "./localStorage.type";

/**
 * Initial state object
 */
const initialState: LocalStorage = {};

/**
 * Thunks are used to dispatch actions that return functions rather than objects,
 * usually used for making api calls or dispatching async actions.
 * Thunks are dispatched in the same way regular actions are dispatched.
 * A slice can have multiple thunks
 */
const makeLocalStorageApiCall = createAsyncThunk(
  // TODO change this method based on usecase
  // You can add as many thunks as required
  // Delete this method if not needed
  "localStorage/makeLocalStorageApiCallStatus",
  async (request: any) => {
    // Make your API call here
  }
);

/**
 * Feature slice Object
 * Automatically generates actions as per reducers
 */
const localStorageSlice = createSlice({
  /**
   * Unique feature name
   */
  name: "redux",

  /**
   * Initial state object
   */
  initialState: initialState,

  reducers: {
    reset: () => initialState,
    // Add here reducers
  },
  /**
   * Extra reducers are for handling action types.
   * Here thunk actions are handled
   */
  extraReducers: (builder) => {
    // TODO remove extraReducers if there are no thunks
    builder.addCase(makeLocalStorageApiCall.pending, (state, action) => {
      // Write pending logic here
    });
    builder.addCase(makeLocalStorageApiCall.fulfilled, (state, action) => {
      // Write success logic here
    });
    builder.addCase(makeLocalStorageApiCall.rejected, (state, action) => {
      // Write failure logic here
    });
  },
});
```
components structure

`import React from 'react';
    import { View, Text, StyleSheet } from 'react-native';

    interface Props {
      /** A text to be displayed in the center of this component */
      title: string;
    }

    const componentName = (props: Props) => {
      const { title } = props;
      return (
        <View style={styles.conatiner}>
          <Text>{title}</Text>
        </View>
      );
    };

    const styles = StyleSheet.create({
      conatiner: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
      },
    });

    export { componentName} ;

```

#### containers

` `import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const containerName = () => {
return (
<View style={styles.container}>
<Text>containerName</Text>
</View>
);
};

const styles = StyleSheet.create({
container: {
alignItems: 'center',
flex: 1,
justifyContent: 'center',
},
});

export { containerName};
`;

```

```

#### assets

`app/assets`

##### Fonts

Put all of your fonts in the `app/assets/fonts` then run `yarn link` to add those fonts to the android and ios assets (we are running manual link since react native auto linking links the packages that are being downloaded by yarn). The path to fonts is specified in the `react-native.config.js` file.

##### Images

When dealing with images it is better to use svg format, to add an svg image simply import it

```js
import CardImg from "&assets/images/buttom_tab/cards.svg";
```

Then use it as a tsx tag

```jsx
<View>
  <HomeImg />;
</View>
```

If you want to change its colors you have to edit the svg, the `.svgrrc` contains the configuration on what properties we can replace

```json
{
  "replaceAttrValues": {
    "#000": "{props.fill}",
    "1": "{props.opacity}",
    "#FFF": "{props.stroke}"
  }
}
```

Edit the svg file, change the fill, stroke and/or opacity as needed.

Before:

```xml
<svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="none" viewBox="0 0 35 35">
    <g fill="#4B4B4B" stroke-width="2">
        <rect width="23" height="19" x="6" y="9" rx="2" />
        <rect width="23" height="14" x="6" y="14" rx="2" />
    </g>
</svg>
```

After:

```xml
<svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" fill="none" viewBox="0 0 35 35">
    <g fill="#000" stroke-width="2">
        <rect width="23" height="19" x="6" y="9" rx="2" />
        <rect width="23" height="14" x="6" y="14" rx="2" />
    </g>
</svg>
```

After we changed the fill to `#000`, the svg transformer will replace all the occurance of `#000` with the provided fill property as below

```jsx
<HomeImg fill={"#FF0000"} />
```

#### api

`app/api`

- The folder that holds the api calles in details, we write the logic of the api call and export a simple function to be called easily in the container.

Example:

```js
import Axois from "axios";

const getStarWarsCharacter = async (id: number) => {
  let result;
  try {
    result = await Axois.get(`https://swapi.dev/api/people/${id}/`);
  } catch (error) {
    result = {
      data: {
        error: "Failed",
      },
    };
  }

  return result.data;
};

export { getStarWarsCharacter };
```

#### redux

`app/redux`

- We'll use redux as a global state managment, if you feel a state should be shared with multiple screens or components then save the state in redux, e.g: sharing the username with other screens.

Example:

- dispatch (set state)

```js
import { useDispatch } from 'react-redux';
const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setUsername(loadData('username')));
  }, [dispatch]);
.
.
.
```

- useSelector (get state)

```js
import { useSelector } from 'react-redux';

const HomeContainer = () => {
  const { username } = useSelector((state: any) => state.kyc);
.
.
.

```

#### redux slices

Example:

```ts
const articleSlice = createSlice({
  name: 'article',

  initialState: initialState,

  reducers: {
    reset: () => initialState,
  },

  extraReducers: (builder) => {
    builder.addCase(getArticles.pending, (state, { meta: { arg: { isPaging } }, payload }) => {

      (!isPaging) ? state.articles = { ...state.articles, isPaging: false, isLoading: true } : state.articles = { ...state.articles, isPaging: true, isLoading: false }

    }),

      builder.addCase(getArticles.fulfilled, (state, { meta: { arg: { isPaging, page } }, payload }) => {

        (!isPaging) ?
          state.articles = {
            page: 1,
            docs: payload.docs,
            isLoading: false,
            isPaging: false
          }
          :
          state.articles = {
            page: page,
            docs: [...state.articles.docs, ...payload.docs],
            isPaging: false,
            isLoading: false
          }
      });

    builder.addCase(getArticles.rejected, (state, action) => {
      state.articles = {
        ...state.articles,
        isPaging: false,
        isLoading: false
      }
    })
  }

})
```

#### redux actions

fire functions

Example:

```ts
import axios from "axios"

export const fetchAricales = (term: string, page: number, field: [string]) => {

  let fl = "fl="

  field?.forEach((item) => { return fl += "," + item })


  return axios.get(`https://api.nytimes.com/svc/search/v2/articlesearch.json?q=${term}&page=${page}&limit=20&sort=newest&${fl}&api-key=OAD0Qz0csaoDZLpw5ZR74TCeSjynnabJ`)

}
```

#### redux types

`app/redux/types`

- we define the object style.

Example:

```ts
export interface Article {

  articles: {
    page: Number,
    docs: string[],
    isLoading: boolean,
    isPaging: boolean
  }
}

```


#### utils

`app/utils`

- Helper functions that might be useful for multiple files/containers, such as concatination of 2 json objects, hashing function, ...

#### config

`app/config`

- global projects configuration that will be used throught out the project, such as colors and strings.

#### cli

`cli`

- command line tool for generating components or containers.
- run `cd cli`to switch to cli folder `node execute` to start the interactive cli.

## Coding Conventions

- The imports should be divided to 3 sections each are sperated with an empty line

1. imports from yarn packages
2. imports from the project
3. imports assets

Example:

```js
import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { HomeContainer } from "&containers/tabs/home.container";
import { ApiExampleContainer } from "&containers/tabs/apiExample.container";

import HomeImg from "&assets/images/buttom_tab/catalogue_main.svg";
import CardImg from "&assets/images/buttom_tab/cards.svg";
```

- Your code should be seperated into related chunks sperated by an empty line

Example:

````js
const OnboardingContainer = () => {
  /* Section for states declaration */
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  /* local utils section */
  const storeCredentials = () => {
    storeData({ username });
    storeData({ password });
  };

  /* Return section */
  return (
    <OnboardingComponent
      buttonHandler={() => {
        storeCredentials();
      }}
      buttonTitle="Click me!"
      onUsernameChange={(text) => setUsername(text)}
      onPasswordChange={(text) => setPassword(text)}
    />
  );
};


- Use async/await not promises

```ts
// Simple and clean
const apiCall = async (url, body) => {
  try {
    return await axios.get(url, body);
  } catch (err) {
    throw err;
  }
};

// Complicated and hard to follow
const apiCall = (url, body) => {
  axios
    .get(url, body)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      if (err) throw err;
    });
};
````

- Document your work, so that other developers could use your code without the need to read it line by line

```ts
// Now when other developers in your team import this component, the ide will suggest those fields
// and the description that you provided for each field will be shown.
interface Props {
  /** A text to be displayed in the center of this component */
  title?: string;

  /** A text to be displayed on the button */
  buttonTitle: string;

  /** A function to be invoked when the button is pressed */
  buttonHandler: () => void;

  /** on username change */
  onUsernameChange: (text: string) => void;

  /** on password change */
  onPasswordChange: (text: string) => void;
}

const OnboardingComponent = (props: Props) => {
  .
  .
  .
}

export { OnboardingComponent };
```

- Use named export not export default

```js
// Accepted
export { TitleBar };

// Rejected
export default TitleBar;
```

## Features

- redux global state managment
- cli tool for files generation
- path alias
- using typescirpt instead of javascript

## Suggestions

#### vs code extentions

- Bable JavaScript
- DotENV
- ESLint
- Git Blame
- GitLense - Get supercharged
- JavaScript (ES6) code snippets
- npm
- npm Intellisense
- Path intellisense
- Pritter - Code formatter
- Visual Studio intelliCode
- yarn
- Todo Tree
- Better comments
  also you can check inside vscode for settings.json then download the missed extensions to have the same coding structure

import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { trackPromise } from 'react-promise-tracker';

import { Article } from '../types/articles';
import { fetchAricales } from '../actions/articles';

const initialState: Article = {
  articles: {
    page: 0,
    docs: [],
    isLoading: false,
    isPaging: false
  }
};

export const getArticles = createAsyncThunk(
  'article/articleStatus',
  async ({ term, page, fields }: any, { rejectWithValue, dispatch }: any) => {
    try {

      const response = await trackPromise(fetchAricales(term, page, fields));

      return response.data.response;
    } catch (e) {
      return rejectWithValue(e.response?.data);
    }
  },
);

const articleSlice = createSlice({
  name: 'article',

  initialState: initialState,

  reducers: {
    reset: () => initialState,
  },

  extraReducers: (builder) => {
    builder.addCase(getArticles.pending, (state, { meta: { arg: { isPaging } }, payload }) => {

      (!isPaging) ? state.articles = { ...state.articles, isPaging: false, isLoading: true } : state.articles = { ...state.articles, isPaging: true, isLoading: false }

    }),

      builder.addCase(getArticles.fulfilled, (state, { meta: { arg: { isPaging, page } }, payload }) => {

        (!isPaging) ?
          state.articles = {
            page: 1,
            docs: payload.docs,
            isLoading: false,
            isPaging: false
          }
          :
          state.articles = {
            page: page,
            docs: [...state.articles.docs, ...payload.docs],
            isPaging: false,
            isLoading: false
          }
      });

    builder.addCase(getArticles.rejected, (state, action) => {
      state.articles = {
        ...state.articles,
        isPaging: false,
        isLoading: false
      }
    })
  }

})

export const articlesReducer = articleSlice.reducer;

export const articleActions = {
  ...articleSlice.actions,
  getArticles,
};

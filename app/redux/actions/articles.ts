import axios from "axios"

export const fetchAricales = (term: string, page: number, field: [string]) => {

  let fl = "fl="

  field?.forEach((item) => { return fl += "," + item })


  return axios.get(`https://api.nytimes.com/svc/search/v2/articlesearch.json?q=${term}&page=${page}&limit=20&sort=newest&${fl}&api-key=OAD0Qz0csaoDZLpw5ZR74TCeSjynnabJ`)

}

export interface Article {

  articles: {
    page: Number,
    docs: string[],
    isLoading: boolean,
    isPaging: boolean
  }
}







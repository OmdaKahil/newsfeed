import { configureStore, getDefaultMiddleware, isImmutableDefault } from '@reduxjs/toolkit';
import {
    persistReducer,
    persistStore,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { rootReducer } from './rootReducer';
import thunk from "redux-thunk";


const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whiteList: [],
    blackList: ['article'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
    reducer: persistedReducer,
    middleware: [thunk]

});

const persistor = persistStore(store);

export type AppDispatch = typeof store.dispatch;
export { store, persistor };
export type RootState = ReturnType<typeof store.getState>;

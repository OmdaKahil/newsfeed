import { articlesReducer } from '&app/redux/slices/articles';
import { combineReducers } from '@reduxjs/toolkit';


const rootReducer = combineReducers({
    /* [TOKEN]:REDUCERS - Do not remove */
    article: articlesReducer,
});

export type RootStateType = ReturnType<typeof rootReducer>;

export { rootReducer };

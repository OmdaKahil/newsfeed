export const urlTransformer = (item: any) => {
  const url = item?.multimedia[0]?.url
    ? 'https://www.nytimes.com/' + item.multimedia[0]?.url
    : 'https://pngimg.com/uploads/question_mark/question_mark_PNG129.png';
  return url
}

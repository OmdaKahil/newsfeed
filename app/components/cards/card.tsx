import React, { useState } from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {
  Text,
  StyleSheet,
  ImageSourcePropType,
  Animated,
  Pressable,
} from "react-native";
import FastImage from "react-native-fast-image";

import { colorPalette } from "&app/assets/colors/colors";

interface Props {
  /** A text to be displayed in the center of this component */
  title: string;

  /**A text holding the uri of the image */
  uri: ImageSourcePropType;

  /** function to handle each card pressed */
  handleCardPress: Function;
}

const Card = (props: Props) => {
  const { uri, title, handleCardPress } = props;
  const [animatedPress] = useState(new Animated.Value(1));

  const animateIn = () => {
    Animated.timing(animatedPress, {
      toValue: 0.9,
      duration: 200,
      useNativeDriver: true,
    }).start();
  };

  const animateOut = () => {
    Animated.timing(animatedPress, {
      toValue: 1,
      duration: 200,
      useNativeDriver: true,
    }).start();
  };

  return (
    <Pressable
      onPressIn={() => {
        animateIn();
      }}
      onPressOut={() => {
        animateOut();
      }}
      onPress={() => {
        handleCardPress();
      }}
      style={styles.mainContainer}
    >
      <Animated.View
        style={[styles.image, { transform: [{ scale: animatedPress }] }]}
      >
        <FastImage
          source={uri}
          style={styles.image}
          resizeMode={FastImage.resizeMode.cover}
        />
      </Animated.View>
      <Animated.View
        style={[styles.bottomBar, { transform: [{ scale: animatedPress }] }]}
      >
        <Text style={styles.title}>
          {title.length > 20 ? title.substring(0, 120 - 3) + "..." : title}
        </Text>
      </Animated.View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    borderRadius: 7,
    shadowColor: "#D9D9D9",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.25,
    width: wp("90%"),
    elevation: 5,

    height: hp("45%"),
    alignSelf: "center",
    marginBottom: 30,
  },
  image: {
    width: "100%",
    height: "100%",
    borderRadius: 7,
  },
  bottomBar: {
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7,
    elevation: 5,
    height: "30%",
    position: "absolute",
    width: "100%",
    backgroundColor: "#D9D9D9",
    bottom: 0,
    opacity: 0.8,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontFamily: "Gotham-Bold",
    color: colorPalette.black,
    fontSize: 14,
    textAlign: "center",
  },
});

export { Card };

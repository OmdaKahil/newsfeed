import React from "react";
import { View, Text, StyleSheet, Pressable } from "react-native";

import { colorPalette } from "&app/assets/colors/colors";

import ShareIcon from "&assets/icons/icon_share.svg";

interface Props {
  /** String holding the title of the button */
  title: string;

  onPress: Function;
}

const RoundedButton = (props: Props) => {
  const { title, onPress } = props;
  return (
    <Pressable
      onPress={() => {
        onPress();
      }}
      style={styles.mainContainer}
    >
      <ShareIcon />
      <View style={styles.seperator}></View>
      <Text>{title}</Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    padding: 5,
    paddingHorizontal: 10,
    borderRadius: 20,
    backgroundColor: colorPalette.greenyBlue,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  seperator: {
    marginHorizontal: 4,
  },
});

export { RoundedButton };

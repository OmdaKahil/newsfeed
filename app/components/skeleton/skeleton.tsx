import React from "react";
import { View, StyleSheet } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

const Skeleton = () => {
  return (
    <SkeletonPlaceholder>
      <View style={styles.mainContainer}>
        <View style={[styles.image]}>
          <View style={styles.image} />
        </View>
        <View style={[styles.bottomBar]} />
      </View>
    </SkeletonPlaceholder>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    borderRadius: 7,
    shadowColor: "#D9D9D9",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.25,
    width: wp("90%"),
    elevation: 5,

    height: hp("45%"),
    alignSelf: "center",
    marginBottom: 30,
  },
  image: {
    width: "100%",
    height: "100%",
    borderRadius: 7,
  },
  bottomBar: {
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7,
    elevation: 5,
    height: "30%",
    position: "absolute",
    width: "100%",
    backgroundColor: "#D9D9D9",
    bottom: 0,
    opacity: 0.5,
  },
});

export { Skeleton };

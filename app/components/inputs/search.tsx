import React from "react";
import { View, Pressable, StyleSheet, TextInput } from "react-native";

import { colorPalette } from "&app/assets/colors/colors";

import SearchIcon from "&assets/icons/search-icon.svg";

interface Props {
  /** Function to handling text input change */
  onChangeText: Function;

  onSearchPressed: Function;
}

const Search = (props: Props) => {
  const { onChangeText, onSearchPressed } = props;
  return (
    <View style={styles.mainContainer}>
      <TextInput
        onChangeText={(value) => {
          onChangeText(value);
        }}
        style={styles.inputLabel}
        placeholder="Search"
        textAlignVertical="center"
        returnKeyType="search"
        onSubmitEditing={() => {
          onSearchPressed();
        }}
      />
      <Pressable
        style={styles.searchIcon}
        onPress={() => {
          onSearchPressed();
        }}
      >
        <SearchIcon />
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  inputLabel: {
    fontFamily: "Gotham-BookItalic",
    fontSize: 16,
    marginLeft: 10,
    width: "85%",
    height: 40,
    color: colorPalette.black,
  },
  searchIcon: {
    justifyContent: "center",
    marginLeft: 10,
  },
  mainContainer: {
    marginTop: 10,
    marginBottom: 15,
    alignSelf: "center",
    flexDirection: "row",
    width: "90%",
    borderRadius: 13,
    shadowColor: colorPalette.greyTransarent,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    backgroundColor: colorPalette.white,
  },
});

export { Search };

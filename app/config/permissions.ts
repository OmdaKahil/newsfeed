import { request, requestMultiple, PERMISSIONS, checkNotifications, requestNotifications } from 'react-native-permissions';
import { Platform } from 'react-native';

export const requestFaceIDPermission = async () => {
    if (Platform.OS === 'ios') {
        return request(PERMISSIONS.IOS.FACE_ID);
    }
};

export const requestCameraPermission = async () => {
    if (Platform.OS === 'ios') {
        return request(PERMISSIONS.IOS.CAMERA);
    } else {
        return request(PERMISSIONS.ANDROID.CAMERA);
    }
};

export const requestReadPhoneStatePermission = async () => {
    if (Platform.OS === 'ios') {
        return;
    } else {
        return request(PERMISSIONS.ANDROID.READ_PHONE_STATE);
    }
};

export const requestGetAccountsPermission = async () => {
    if (Platform.OS === 'ios') {
        return;
    } else {
        return request(PERMISSIONS.ANDROID.GET_ACCOUNTS);
    }
};

export const requestSmsPermission = async () => {
    if (Platform.OS === 'ios') {
        return;
    } else {
        return requestMultiple([PERMISSIONS.ANDROID.READ_SMS, PERMISSIONS.ANDROID.RECEIVE_SMS]);
    }
};


export const checkNotification = async () => {
    if (Platform.OS === 'ios') {
        return checkNotifications().then(({ status }) => {
            return Promise.resolve(status)
        })
    }
}


export const requestNotification = async () => {
    if (Platform.OS === 'ios') {
        return requestNotifications(['alert', 'sound']).then(({ status, settings }) => {
        });
    }
}


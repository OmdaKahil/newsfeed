import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import memoize from 'lodash.memoize';
import { I18nManager } from 'react-native';

const translationGetters: any = {
    // lazy requires (metro bundler does not support symlinks)
    ar: () => require('../assets/localization/ar.json'),
    en: () => require('../assets/localization/en.json'),
    tr: () => require('../assets/localization/tr.json'),
};

export const wordsKit: any = memoize(
    (key: any, config: any) => i18n.t(key, config),
    (key: any, config: any) => (config ? key + JSON.stringify(config) : key),
);

export const setI18nConfig = () => {
    // fallback if no available language fits
    const fallback = { languageTag: 'en', isRTL: false };

    const { languageTag, isRTL } =
        RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
        fallback;

    // clear translation cache
    wordsKit.cache.clear();
    // update layout direction
    I18nManager.forceRTL(isRTL);
    // set i18n-js config
    i18n.translations = { [languageTag]: translationGetters[languageTag]() };
    i18n.locale = languageTag;
};

export const colorPalette = {
    white: '#FFFFFF',
    greyTransarent: '#000',
    black: 'black',
    greenyBlue: 'rgb(22,122,141)',
};

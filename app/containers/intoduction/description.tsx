import { useRoute } from "@react-navigation/native";
import React from "react";
import Share from "react-native-share";
import {
  View,
  StyleSheet,
  Image,
  Text,
  Linking,
  ScrollView,
} from "react-native";
import { heightPercentageToDP } from "react-native-responsive-screen";
import { SafeAreaView } from "react-native-safe-area-context";
import { useSelector } from "react-redux";

import { RoundedButton } from "&app/components/buttons/roundedButton";
import { urlTransformer } from "&app/utils/urlTransformer";
import { colorPalette } from "&app/assets/colors/colors";

const Description = () => {
  const route = useRoute();
  const article = useSelector(
    (state: any) => state.article.articles.docs[route?.params?.index]
  );

  const share = async () => {
    Share.open({
      message: article?.web_url,
      title: "Share",
    })
      .then((response) => {})
      .catch((e) => {});
  };

  const handleUrlPress = () => {
    Linking.canOpenURL(article.web_url).then((supported) => {
      if (supported) {
        Linking.openURL(article.web_url);
      } else {
        console.log("Url Couldn't be opened: " + article.web_url);
      }
    });
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <Image
        resizeMode={"stretch"}
        source={{
          uri: urlTransformer(article),
        }}
        style={styles.image}
      />
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.authorContainer}>
            <Text style={styles.author}>{article.byline?.original}</Text>
            <RoundedButton title={"Share"} onPress={share} />
          </View>
          <Text style={styles.descTitle}>Abstract:</Text>
          <Text style={styles.paragraph}>{article.abstract}</Text>
          <Text style={styles.descTitle}>Description:</Text>
          <Text style={styles.paragraph}>{article.lead_paragraph}</Text>
          <Text style={styles.descTitle}>
            check the link below for more info:
          </Text>
          <Text onPress={handleUrlPress} style={styles.url}>
            {article.web_url}
          </Text>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  descTitle: {
    marginTop: 10,
    fontFamily: "Gotham-Bold",
  },
  url: {
    marginTop: 10,
    color: "blue",
  },
  author: {
    fontFamily: "Gotham-BookItalic",
    width: "70%",
  },
  mainContainer: {
    flex: 1,
  },
  container: {
    borderRadius: 5,
    shadowColor: "#0000",
    shadowOffset: {
      width: 0,
      height: 12,
    },
    height: heightPercentageToDP("54%"),
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 24,
    margin: 10,
    padding: 10,
    backgroundColor: colorPalette.white,
  },
  image: {
    width: "100%",
    height: heightPercentageToDP("40%"),
  },
  authorContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  paragraph: {
    marginVertical: 10,
    lineHeight: 23,
  },
});

export { Description };

import React from "react";
import { useEffect, useRef } from "react";
import { StyleSheet, SafeAreaView, FlatList } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";

import { Search } from "&app/components/inputs/search";
import { Card } from "&app/components/cards/card";
import { getArticles } from "&app/redux/slices/articles";
import { urlTransformer } from "&app/utils/urlTransformer";
import { Skeleton } from "&app/components/skeleton/skeleton";

const Home = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const fileds = [
    "abstract",
    "web_url",
    "lead_paragraph",
    "multimedia",
    "byline",
  ];
  const articles = useSelector((state: any) => state.article.articles);
  const term = useRef<string>("");
  const isLoading = useSelector(
    (state: any) => state.article.articles.isLoading
  );
  const isPaging = useSelector((state: any) => state.article.articles.isPaging);

  const handleCardPress = (index: number) => {
    navigation.navigate("Description", { index: index });
  };

  const onSearchTextChange = (text: string) => (term.current = text);

  const onSearchPressed = () =>
    dispatch(
      getArticles({
        term: term.current,
        page: 1,
        fields: fileds,
        isPaging: false,
      })
    );

  const renderItem = ({ item, index }: any) => {
    return (
      <Card
        key={index}
        handleCardPress={() => {
          handleCardPress(index);
        }}
        title={item.abstract}
        uri={{ uri: urlTransformer(item) }}
      />
    );
  };

  const renderFooter = (key: any) => {
    return isPaging ? (
      <>
        <Skeleton />
        <Skeleton />
      </>
    ) : null;
  };

  useEffect(() => {
    dispatch(
      getArticles({
        term: term.current,
        page: 1,
        fields: fileds,
        isPaging: false,
      })
    );
  }, []);

  const onEndReached = () => {
    if (!isPaging) {
      dispatch(
        getArticles({
          term: term.current,
          page: articles.page + 1,
          fields: fileds,
          isPaging: true,
        })
      );
    }
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <Search
        onChangeText={onSearchTextChange}
        onSearchPressed={onSearchPressed}
      />
      {isLoading ? (
        <>
          <Skeleton />
          <Skeleton />
        </>
      ) : (
        <FlatList
          contentContainerStyle={{ paddingBottom: 50 }}
          keyExtractor={(item) => item.index}
          onEndReachedThreshold={0.1}
          onEndReached={({ distanceFromEnd }) => {
            onEndReached();
          }}
          renderItem={renderItem}
          data={articles.docs}
          ListFooterComponent={renderFooter}
        />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: "#D9D9D9",
    flex: 1,
  },
  midLoader: {
    backgroundColor: "#D9D9D9",
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  loadingView: {
    height: 20,
    alignItems: "center",
  },
  activityIndicator: {
    alignItems: "center",
    justifyContent: "center",
  },
});

export { Home };

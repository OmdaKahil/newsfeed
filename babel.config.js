module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['.'],
        extensions: [
          '.ios.ts',
          '.android.ts',
          '.ts',
          '.ios.tsx',
          '.android.tsx',
          '.tsx',
          '.jsx',
          '.js',
          '.json',
          '.svg',
        ],
        alias: {
          '&app': './app',
          '&redux': './app/redux',
          '&config': './app/config',
          '&containers': './app/containers',
          '&components': './app/components',
          '&assets': './app/assets',
          '&navigator': './app/navigator',
          '&store': './app/store',
          '&navigator': './app/navigator',
        },
      },
    ],
  ],
};
